---
title: Useful Linux Tools
---

## Terminal Emulation/Useful for Terminal

* [Terminator Terminal Emulator](https://gnome-terminator.org/) -
  Nice way to split terminals, etc.
